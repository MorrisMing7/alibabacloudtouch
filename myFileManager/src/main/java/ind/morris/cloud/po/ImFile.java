package ind.morris.cloud.po;

import ind.morris.cloud.common.po.BaseRecord;
import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Data
public class ImFile extends BaseRecord {
    Long id;
    String md5sum;
    String fileName;
    String fileOriName;
    Long fileSize;
    String savePath;
    String type;
}
