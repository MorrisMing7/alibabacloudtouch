package ind.morris.cloud.feign;

import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FeignFallBack implements FallbackFactory<FeignDataParser> {
    @Override
    public FeignDataParser create(Throwable throwable) {
        return new FeignDataParser() {
            @Override
            public String echo(String p) {
                log.error("echo 接口错误：{}",throwable.getClass().toString());
                if(throwable instanceof FlowException) {
                    return "接口被流控了";
                }
                else if(throwable instanceof DegradeException) {
                    return "服务被降级了！";
                }
                else {
                    return "其他异常了！";
                }
            }
        };
    }
}
