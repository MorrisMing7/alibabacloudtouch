package ind.morris.cloud.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "data-parser", fallbackFactory = FeignFallBack.class)
public interface FeignDataParser {
    @GetMapping("/echo/{p}")
    String echo(@PathVariable String p);



}
