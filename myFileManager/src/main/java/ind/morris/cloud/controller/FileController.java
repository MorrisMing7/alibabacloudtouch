package ind.morris.cloud.controller;

import ind.morris.cloud.feign.FeignDataParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/file")
public class FileController {
    @Autowired
    private FeignDataParser feignDataParser;
    @GetMapping("/notAFile")
    public String naf(String x){
        return "not a file "+ feignDataParser.echo("not a file");
    }
}
