package ind.morris.cloud.common.exception;

import com.alibaba.cloud.sentinel.rest.SentinelClientHttpResponse;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.fastjson.JSONObject;
import ind.morris.cloud.common.vo.SomeMsg;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;

public class GlobalExceptionHandler {
    /**
     * 限流的处理方法
     */
    public static SentinelClientHttpResponse flowLimitHandler(HttpRequest request, byte[] body,
                                                              ClientHttpRequestExecution execution, BlockException ex) {
        SomeMsg f = new SomeMsg(-1, "被限流了!", 0);
        return new SentinelClientHttpResponse(JSONObject.toJSONString(f));
    }

    /**
     * 服务降级
     * @return
     */
    public static SentinelClientHttpResponse reduceHandler(HttpRequest request, byte[] body,
                                                           ClientHttpRequestExecution execution, BlockException ex) {
        SomeMsg f = new SomeMsg(-1, "被限流了!", 0);
        return new SentinelClientHttpResponse(JSONObject.toJSONString(f));
    }
}
