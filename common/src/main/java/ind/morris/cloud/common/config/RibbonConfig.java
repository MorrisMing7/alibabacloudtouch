package ind.morris.cloud.common.config;

import com.alibaba.cloud.sentinel.annotation.SentinelRestTemplate;
import ind.morris.cloud.common.exception.GlobalExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Configuration
public class RibbonConfig {

    @Bean
    @LoadBalanced
    @SentinelRestTemplate(
            blockHandler = "flowLimitHandler", blockHandlerClass = GlobalExceptionHandler.class,
            fallback = "reduceHandler", fallbackClass = GlobalExceptionHandler.class
    )
    public RestTemplate restTemplate() {
        log.info("init rest template!!!!!!!!!!!!!");

        return new RestTemplate();
    }
}
