package ind.morris.cloud.common.vo;

import lombok.Data;

@Data
public class SomeMsg {
    private final Integer state;
    private final String msg;
    private final Integer code;

    public SomeMsg(int state, String msg, int code) {
        this.state = state;
        this.msg = msg;
        this.code = code;
    }
}
