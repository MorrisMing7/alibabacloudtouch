package ind.morris.cloud.common.po;

import lombok.Data;

import java.util.Date;

@Data
public class BaseRecord {
    private Long id;
    private Date createTime;
    private Date updateTime;
    private BaseRecordStatus rowStatus = BaseRecordStatus.Normal;
}
