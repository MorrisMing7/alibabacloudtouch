package ind.morris.cloud.dataParser.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/echo")
@Slf4j
public class EchoController {

    @GetMapping("/{p}")
    public String echo(@PathVariable String p){
        log.info("echoing: {}",p);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ignored) { }
        return "echo: "+p;
    }

    @GetMapping("/divide")
    public String divide(@RequestParam Integer a, @RequestParam Integer b) {
        return String.valueOf(a / b);
    }
}
