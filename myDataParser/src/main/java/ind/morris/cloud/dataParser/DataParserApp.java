package ind.morris.cloud.dataParser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class DataParserApp {
    public static void main(String[] args) {
        SpringApplication.run(DataParserApp.class, args);
    }
}
