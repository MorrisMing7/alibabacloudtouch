###nacos
*nacos-server-2.0.4.tar.gz*
```shell
sh bin/startup.sh  -m standalone
```

###sentinel
*sentinel-dashboard-1.8.3*
``` shell
java -server -Xms64m -Xmx256m -Dserver.port=8849 -Dcsp.sentinel.dashboard.server=localhost:8849 -Dproject.name=sentinel-dashboard -jar sentinel-dashboard-1.8.3.jar
```